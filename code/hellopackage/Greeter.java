package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        int value = scan.nextInt();
        System.out.println(Utilities.doubleMe(value));
    }
}